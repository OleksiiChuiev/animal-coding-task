# Animal Coding task
## To build and execute application:
* Clone repository:
```
git clone https://OleksiiChuiev@bitbucket.org/OleksiiChuiev/animal-coding-task.git
```
* Change directory:
```
cd animal-coding-task
```
* Build project:
```
mvn clean install
```
* Change directory:
```
cd target
```
* Execute jar file:
```
java -jar animal-coding-task-1.0-SNAPSHOT.jar
```

## Notes:
* File input2.txt is situated in resources and bundled into the jar file.
* If you want to process additional files you could specify paths to files as a startup parameters:
```
java -jar animal-coding-task-1.0-SNAPSHOT.jar {absolutePathToFile} {absolutePathToFile}
```

## Why Spring Boot is used:
* Easy to build fully executable jar - Spring boot handles all resources, dependencies, etc.
* Spring DI - using spring dependency injection mechanism allows us to build application in which
if you want to add new categories handling you just need to specify additional CategoryDescriptor
and it will be automatically used during file processing without any changes in base app logic.
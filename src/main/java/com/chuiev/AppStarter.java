package com.chuiev;

import com.chuiev.api.CategoriesFiller;
import com.chuiev.api.CategoriesOutputWriter;
import com.chuiev.filereader.AbsolutePathFileReader;
import com.chuiev.filereader.ResourceFileReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Application starter which process predefined in resources file and all files read by the passed paths.
 *
 * @author Oleksii_Chuiev
 */
@Component
class AppStarter {

    private static final String EMBEDDED_FILE_NAME = "input2.txt";

    @Autowired
    private CategoriesFiller categoriesFiller;

    @Autowired
    private CategoriesOutputWriter categoriesOutputWriter;

    @Autowired
    private ResourceFileReader resourceFileReader;

    @Autowired
    private AbsolutePathFileReader absolutePathFileReader;

    /**
     * Starts the application logic which process predefined in resources file and all files read by the passed paths.
     *
     * @param absoluteFilePaths absolute files paths which should be processed by the app logic.
     */
    void startApp(String[] absoluteFilePaths) {
        Stream.of(resourceFileReader.readFile(EMBEDDED_FILE_NAME),
                Arrays.stream(absoluteFilePaths).flatMap(path -> absolutePathFileReader.readFile(path)))
                .flatMap(element -> element)
                .forEach(line -> categoriesFiller.fillCategoryByLine(line));

        categoriesOutputWriter.outputCategories();
    }

}

package com.chuiev.base.formatter;

import com.chuiev.api.OutputFormatter;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * An implementation of {@link OutputFormatter} which formats and converts categories values
 * and related useful data into the single readable output value.
 *
 * @author Oleksii_Chuiev
 */
public class CategoryOutputFormatter implements OutputFormatter {

    private static final String COLON = ":";

    private String categoryName;

    public CategoryOutputFormatter(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * Formats and converts categories entries (lines) into the single readable output value.
     *
     * @param lines categories entries (lines).
     *
     * @return formatted and converted into single output value stream of categories entries in format
     * <p>categoryName:</p>
     * <p>categoryValue1</p>
     * <p>categoryValue2</p>
     * <p>categoryValue3</p>
     */
    @Override
    public String format(Stream<String> lines) {
        return categoryName.toUpperCase() +
                COLON +
                System.lineSeparator() +
                lines.collect(Collectors.joining(System.lineSeparator()));
    }

}

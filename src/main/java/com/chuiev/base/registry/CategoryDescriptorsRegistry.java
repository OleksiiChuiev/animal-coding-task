package com.chuiev.base.registry;

import com.chuiev.base.descriptor.CategoryDescriptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Registry which provides existing in app categoru descriptors.
 *
 * @author Oleksii_Chuiev
 */
@Component
public class CategoryDescriptorsRegistry {

    private List<CategoryDescriptor> categoryDescriptors;

    /**
     * Creates a new instance of {@link CategoryDescriptorsRegistry}.
     *
     * @param categoryDescriptors list of category descriptors.
     */
    @Autowired
    public CategoryDescriptorsRegistry(List<CategoryDescriptor> categoryDescriptors) {
        this.categoryDescriptors = categoryDescriptors;
    }

    /**
     * Returns category descriptors.
     *
     * @return list of Category descriptors.
     */
    public List<CategoryDescriptor> getCategoryDescriptors() {
        return categoryDescriptors;
    }

}

package com.chuiev.base.matcher;

import com.chuiev.api.CategoryMatcher;
import org.apache.commons.lang3.StringUtils;

/**
 * {@link CategoryMatcher} implementation which matches category by its name.
 *
 * @author Oleksii_Chuiev
 */
public class CategoryNameMatcher implements CategoryMatcher {

    private String categoryName;

    /**
     * Creates a new instance of {@link CategoryNameMatcher}.
     *
     * @param categoryName name of category.
     */
    public CategoryNameMatcher(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * Matches given string value with exact category name.
     *
     * @param line some string value.
     *
     * @return true if given line matches with exact category name.
     */
    @Override
    public boolean matches(String line) {
        return categoryName.equalsIgnoreCase(StringUtils.trim(line));
    }

}

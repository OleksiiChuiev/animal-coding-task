package com.chuiev.base.outputwriter;

import com.chuiev.api.CategoriesOutputWriter;
import com.chuiev.base.descriptor.CategoryDescriptor;
import com.chuiev.base.registry.CategoryDescriptorsRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Categories output writer which handles values from given category descriptors
 * and writes them to the console output.
 *
 * @author Oleksii_Chuiev
 */
@Component
public class CategoriesConsoleOutputWriter implements CategoriesOutputWriter {

    private CategoryDescriptorsRegistry registry;

    /**
     * Creates a new instance of {@link CategoriesConsoleOutputWriter}.
     *
     * @param registry category descriptors registry.
     */
    @Autowired
    public CategoriesConsoleOutputWriter(CategoryDescriptorsRegistry registry) {
        this.registry = registry;
    }

    /**
     * Writes categories values to the application "standard" output stream.
     * Each category descriptor will be processed by its post processor and output formatter.
     */
    @Override
    public void outputCategories() {
        registry.getCategoryDescriptors().stream()
                .filter(this::isNotEmptyCategory)
                .map(CategoryDescriptor::processValues)
                .forEach(System.out::println);
    }

    private Boolean isNotEmptyCategory(CategoryDescriptor descriptor) {
        return !descriptor.getValues().isEmpty();
    }
}

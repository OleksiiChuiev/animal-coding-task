package com.chuiev.base.descriptor;

import com.chuiev.api.CategoryMatcher;
import com.chuiev.api.OutputFormatter;
import com.chuiev.api.PostProcessor;
import com.chuiev.base.model.CategoryModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Category descriptor which contains all related to category processing objects
 * (e.g. post processor, matcher, etc), category values.
 *
 * @author Oleksii_Chuiev
 */
public class CategoryDescriptor {

    private CategoryMatcher categoryMatcher;
    private OutputFormatter outputFormatter;
    private PostProcessor postProcessor;
    private CategoryModel categoryModel;

    /**
     * Creates a new instance of {@link CategoryDescriptor}.
     *
     * @param name categories name
     * @param categoryMatcher categories matcher.
     * @param outputFormatter categories output formatter.
     * @param postProcessor categories post processor.
     */
    public CategoryDescriptor(String name,
                              CategoryMatcher categoryMatcher,
                              OutputFormatter outputFormatter,
                              PostProcessor postProcessor) {
        this.categoryMatcher = categoryMatcher;
        this.outputFormatter = outputFormatter;
        this.postProcessor = postProcessor;
        this.categoryModel = new CategoryModel(name, new ArrayList<>());
    }

    /**
     * Returns categories name.
     */
    public String getName() {
        return categoryModel.getName();
    }

    /**
     * Returns categories matcher.
     */
    public CategoryMatcher getCategoryMatcher() {
        return categoryMatcher;
    }

    /**
     * Returns categories value.
     */
    public List<String> getValues() {
        return categoryModel.getValues();
    }

    /**
     * Adds given value to the categories values.
     *
     * @param value value which will be added to categories values.
     */
    public void addValue(String value) {
        categoryModel.getValues().add(value);
    }

    /**
     * Process catefory descriptor's values via post processor and output formatter.
     *
     * @return processed category descriptor's values in string representation.
     */
    public String processValues() {
        return outputFormatter.format(postProcessor.process(categoryModel.getValues().stream()));
    }

}

package com.chuiev.base.filler;

import com.chuiev.api.CategoriesFiller;
import com.chuiev.base.descriptor.CategoryDescriptor;
import com.chuiev.base.registry.CategoryDescriptorsRegistry;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Base service for manipulating with categories.
 *
 * @author Oleksii_Chuiev
 */
@Component
public class BaseCategoriesFiller implements CategoriesFiller {

    private CategoryDescriptor currentDescriptor;
    private CategoryDescriptorsRegistry registry;

    /**
     * Creates a new instance of {@link BaseCategoriesFiller}.
     *
     * @param registry category descriptors registry.
     * @param defaultDescriptor default category descriptor which will be used for the first values
     *                          during processing which could not be matched to any other categories.
     */
    @Autowired
    public BaseCategoriesFiller(CategoryDescriptorsRegistry registry, CategoryDescriptor defaultDescriptor) {
        this.registry = registry;
        this.currentDescriptor = defaultDescriptor;
    }

    @Override
    public void fillCategoryByLine(String line) {
        currentDescriptor = registry.getCategoryDescriptors().stream()
                .filter(descriptor -> descriptor.getCategoryMatcher().matches(line))
                .findFirst()
                .orElse(currentDescriptor);

        Optional.ofNullable(line)
                .filter(StringUtils::isNotEmpty)
                .filter(value -> isNotCategoryName(currentDescriptor, value))
                .ifPresent(value -> currentDescriptor.addValue(value));
    }

    private Boolean isNotCategoryName(CategoryDescriptor currentDescriptor, String line) {
        return !currentDescriptor.getName().equalsIgnoreCase(line);
    }

}

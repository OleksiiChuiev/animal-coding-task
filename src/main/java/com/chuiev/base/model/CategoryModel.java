package com.chuiev.base.model;

import java.util.List;

/**
 * This model represents named category with some string values.
 *
 * @author Oleksii_Chuiev
 */
public class CategoryModel {

    private String name;
    private List<String> values;

    /**
     * Creates a new instance of {@link CategoryModel}.
     *
     * @param name categories name
     * @param values categories values
     */
    public CategoryModel(String name, List<String> values) {
        this.name = name;
        this.values = values;
    }

    /**
     * Returns categories name.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns categories value.
     */
    public List<String> getValues() {
        return values;
    }
}

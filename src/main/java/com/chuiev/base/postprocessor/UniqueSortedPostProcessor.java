package com.chuiev.base.postprocessor;

import com.chuiev.api.PostProcessor;

import java.util.stream.Stream;

/**
 * Implementation of {@link PostProcessor} which sorts and removes duplicates values.
 *
 * @author Oleksii_Chuiev
 */
public class UniqueSortedPostProcessor implements PostProcessor {

    /**
     * Sorts in ascending order ans removes duplicates string values.
     *
     * @param values stream category string values.
     *
     * @return sorted stream of category string values without duplicates.
     */
    @Override
    public Stream<String> process(Stream<String> values) {
        return values.distinct().sorted();
    }

}

package com.chuiev.base.postprocessor;

import com.chuiev.api.PostProcessor;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.Comparator;
import java.util.stream.Stream;

/**
 * Implementation of {@link PostProcessor} which turns values to lower case, sorts them in reverse order,
 * removes duplicates and calculate hashes.
 *
 * @author Oleksii_Chuiev
 */
public class ReverseSortedLowercasedUniqueHashedPostProcessor implements PostProcessor {

    private static final String SPACE_WITH_OPEN_ROUND_BRACKET = " (";
    private static final String CLOSED_ROUND_BRACKET = ")";

    /**
     * Converts to lowercase, sorts in descending order, removes duplicates and generates hashes
     * for category string values.
     *
     * @param values stream of category string values.
     *
     * @return sorted in descending order stream of category string values without duplicates
     * with hashes in lowercase.
     */
    @Override
    public Stream<String> process(Stream<String> values) {
        return values.map(String::toLowerCase)
                .distinct()
                .sorted(Comparator.reverseOrder())
                .map(value -> value +
                        SPACE_WITH_OPEN_ROUND_BRACKET +
                        DigestUtils.md5Hex(value) +
                        CLOSED_ROUND_BRACKET);
    }

}

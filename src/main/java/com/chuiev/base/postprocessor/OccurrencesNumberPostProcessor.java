package com.chuiev.base.postprocessor;

import com.chuiev.api.PostProcessor;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Implementation of {@link PostProcessor} for counting number of occurrences for each string value.
 *
 * @author Oleksii_Chuiev
 */
public class OccurrencesNumberPostProcessor implements PostProcessor {

    private static final String COLON_WITH_SPACE = ": ";

    /**
     * Converts given string values into the statistics with number of
     * occurrences of the each data item without duplicates.
     *
     * @param values stream of category string values.
     *
     * @return statistics with number of occurrences of the each data item without duplicates.
     */
    @Override
    public Stream<String> process(Stream<String> values) {
        return values
                .collect(Collectors.groupingBy(value -> value, Collectors.counting()))
                .entrySet()
                .stream()
                .map(entry -> entry.getKey() + COLON_WITH_SPACE + entry.getValue());
    }

}

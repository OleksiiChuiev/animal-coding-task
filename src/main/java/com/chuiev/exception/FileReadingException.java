package com.chuiev.exception;

/**
 * Signals that error during reading file has occurred.
 *
 * @author Oleksii_Chuiev
 */
public class FileReadingException extends RuntimeException {

    /**
     * Constructs an {@link FileReadingException} with the specified detail message
     * and cause.
     *
     * @param message the detail message.
     * @param cause the cause of issue.
     */
    public FileReadingException(String message, Throwable cause) {
        super(message, cause);
    }

}

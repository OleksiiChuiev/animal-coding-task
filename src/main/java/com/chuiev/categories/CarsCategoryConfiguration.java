package com.chuiev.categories;

import com.chuiev.base.descriptor.CategoryDescriptor;
import com.chuiev.base.formatter.CategoryOutputFormatter;
import com.chuiev.base.matcher.CategoryNameMatcher;
import com.chuiev.base.postprocessor.ReverseSortedLowercasedUniqueHashedPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Cars Category configuration.
 *
 * @author Oleksii_Chuiev
 */
@Configuration
public class CarsCategoryConfiguration {

    private static final String NAME = "cars";

    /**
     * Creates a new instance of {@link CategoryDescriptor} as a Bean.
     * It use "cars" name, custom matcher, reverse sorted, unique, lowercased, hashed post processor,
     * output formatter for cars category,
     */
    @Bean
    public CategoryDescriptor carsDescriptor() {
        return new CategoryDescriptor(
                NAME,
                new CategoryNameMatcher(NAME),
                new CategoryOutputFormatter(NAME),
                new ReverseSortedLowercasedUniqueHashedPostProcessor()
        );
    }
}

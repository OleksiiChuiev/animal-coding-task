package com.chuiev.categories;

import com.chuiev.base.descriptor.CategoryDescriptor;
import com.chuiev.base.formatter.CategoryOutputFormatter;
import com.chuiev.base.matcher.CategoryNameMatcher;
import com.chuiev.base.postprocessor.OccurrencesNumberPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Numbers Category configuration.
 *
 * @author Oleksii_Chuiev
 */
@Configuration
public class NumbersCategoryConfiguration {

    private static final String NAME = "numbers";

    /**
     * Creates a new instance of {@link CategoryDescriptor} as a Bean.
     * It use "numbers" name, custom matcher, occurrences number post processor,
     * output formatter for number category,
     */
    @Bean
    public CategoryDescriptor numbersDescriptor() {
        return new CategoryDescriptor(
                NAME,
                new CategoryNameMatcher(NAME),
                new CategoryOutputFormatter(NAME),
                new OccurrencesNumberPostProcessor()
        );
    }
}

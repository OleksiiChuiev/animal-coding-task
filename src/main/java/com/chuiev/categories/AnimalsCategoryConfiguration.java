package com.chuiev.categories;

import com.chuiev.base.descriptor.CategoryDescriptor;
import com.chuiev.base.formatter.CategoryOutputFormatter;
import com.chuiev.base.matcher.CategoryNameMatcher;
import com.chuiev.base.postprocessor.UniqueSortedPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Animals Category configuration.
 *
 * @author Oleksii_Chuiev
 */
@Configuration
public class AnimalsCategoryConfiguration {

    private static final String NAME = "animals";

    /**
     * Creates a new instance of {@link CategoryDescriptor} as a Bean.
     * It use "animals" name, custom matcher, unique sorted post processor, output formatter for animals category,
     */
    @Bean
    public CategoryDescriptor animalsDescriptor() {
        return new CategoryDescriptor(
                NAME,
                new CategoryNameMatcher(NAME),
                new CategoryOutputFormatter(NAME),
                new UniqueSortedPostProcessor()
        );
    }
}

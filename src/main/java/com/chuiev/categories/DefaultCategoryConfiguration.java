package com.chuiev.categories;

import com.chuiev.base.descriptor.CategoryDescriptor;
import com.chuiev.base.formatter.CategoryOutputFormatter;
import com.chuiev.base.matcher.CategoryNameMatcher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Default Category configuration.
 *
 * @author Oleksii_Chuiev
 */
@Configuration
public class DefaultCategoryConfiguration {

    private static final String NAME = "default";

    /**
     * Creates a new instance of {@link CategoryDescriptor} as a Bean.
     * It use "default" name, custom matcher, post processor which do nothing,
     * output formatter for default category,
     */
    @Bean
    public CategoryDescriptor defaultDescriptor() {
        return new CategoryDescriptor(
                NAME,
                new CategoryNameMatcher(NAME),
                new CategoryOutputFormatter(NAME),
                stream -> stream
        );
    }
}

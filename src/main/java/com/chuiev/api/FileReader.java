package com.chuiev.api;

import java.util.stream.Stream;

/**
 * File reader which read file line by line and returns it as a stream.
 *
 * @author Oleksii_Chuiev
 */
public interface FileReader {

    /**
     * Reads file by the given file path line by line and returns it as a stream of lines.
     *
     * @param filePath path to the file.
     *
     * @return stream of read lines from file.
     */
    Stream<String> readFile(String filePath);

}

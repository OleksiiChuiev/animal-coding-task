package com.chuiev.api;

/**
 * Categories output writer which handles values from given category descriptors.
 *
 * @author Oleksii_Chuiev
 */
public interface CategoriesOutputWriter {

    /**
     * Writes categories values to some output stream.
     */
    void outputCategories();

}

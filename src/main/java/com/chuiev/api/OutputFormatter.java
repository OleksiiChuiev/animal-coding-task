package com.chuiev.api;

import java.util.stream.Stream;

/**
 * An interpreter for stream of string which formats given string, collect them into single output string.
 *
 * @author Oleksii_Chuiev
 */
public interface OutputFormatter {

    /**
     * Formats and converts given stream of string into the single output string value.
     *
     * @param lines stream of string.
     *
     * @return formatted and converted into single output value stream of string.
     */
    String format(Stream<String> lines);

}

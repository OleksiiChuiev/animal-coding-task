package com.chuiev.api;

import java.util.stream.Stream;

/**
 * Processor which performs actions on a given steam and returns changed stream.
 *
 * @author Oleksii_Chuiev
 */
public interface PostProcessor {

    /**
     * Performs actions on a given steam and returns it with applied changes.
     *
     * @param values stream of some string values.
     *
     * @return given stream with applied changes.
     */
    Stream<String> process(Stream<String> values);

}

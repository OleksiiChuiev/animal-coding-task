package com.chuiev.api;

/**
 * Filler class which process given data, compare it with categories and fills appropriate
 * category by this data.
 *
 * @author Oleksii_Chuiev
 */
public interface CategoriesFiller {

    /**
     * Matches given string line with given category descriptors and fills appropriate
     * category values by this line.
     *
     * @param line string value which will be matched to the given categories and then
     *             appropriate category will be filled by this line.
     */
    void fillCategoryByLine(String line);

}

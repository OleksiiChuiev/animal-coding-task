package com.chuiev.api;

/**
 * A matcher that performs match operations on a given line to
 * determine the category to which the line belongs.
 *
 * @author Oleksii_Chuiev
 */
public interface CategoryMatcher {

    /**
     * Attempts to match the given line with exact category.
     *
     * @param line some string value.
     *
     * @return true if given line matches with exact category and belongs to this category.
     */
    boolean matches(String line);

}

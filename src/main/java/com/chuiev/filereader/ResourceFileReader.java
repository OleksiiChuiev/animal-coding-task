package com.chuiev.filereader;

import com.chuiev.api.FileReader;
import com.chuiev.exception.FileReadingException;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Stream;

/**
 * Implementation of {@link FileReader} which read file from the bundled resources.
 *
 * @author Oleksii_Chuiev
 */
@Component
public class ResourceFileReader implements FileReader {

    /**
     * Reads file from bundled resources by the given file path line by line
     * and returns it as a stream of lines.
     *
     * @param filePath path to the file in resources.
     *
     * @return stream of read lines from file.
     * @throws FileReadingException in case of errors during file reading.
     */
    @Override
    public Stream<String> readFile(String filePath) {
        try {
            return new BufferedReader(
                    new InputStreamReader(
                            new ClassPathResource(filePath).getInputStream(),
                            StandardCharsets.UTF_8))
                    .lines();
        } catch (IOException e) {
            throw new FileReadingException("Couldn't read file by path: " + filePath, e);
        }
    }

}

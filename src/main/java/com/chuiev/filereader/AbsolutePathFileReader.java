package com.chuiev.filereader;

import com.chuiev.api.FileReader;
import com.chuiev.exception.FileReadingException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Implementation of {@link FileReader} which read file by the absolute path.
 *
 * @author Oleksii_Chuiev
 */
@Component
public class AbsolutePathFileReader implements FileReader {

    /**
     * Reads file by the given absolute file path line by line and returns it as a stream of lines.
     *
     * @param filePath absolute path to the file.
     *
     * @return stream of read lines from file.
     * @throws FileReadingException in case of errors during file reading.
     */
    @Override
    public Stream<String> readFile(String filePath) {
        try {
            return Files.lines(Paths.get(filePath));
        } catch (IOException e) {
            throw new FileReadingException("Couldn't read file by path: " + filePath, e);
        }
    }

}

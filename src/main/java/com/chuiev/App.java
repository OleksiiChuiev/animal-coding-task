package com.chuiev;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Entry point into the Apring boot application.
 * It obtains the application context, finds starter class and starts app business logic.
 *
 * @author Oleksii_Chuiev
 */
@SpringBootApplication
public class App {

    /**
     * Main method which starts the app logic.
     *
     * @param absoluteFilePaths startup parameters corresponded to the absolute files
     *                          paths which should be processed by the app logic.
     */
    public static void main(String[] absoluteFilePaths) {
        ApplicationContext context = new AnnotationConfigApplicationContext(App.class);
        context.getBean(AppStarter.class).startApp(absoluteFilePaths);
    }

}

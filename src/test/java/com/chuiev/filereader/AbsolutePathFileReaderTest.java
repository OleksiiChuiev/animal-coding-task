package com.chuiev.filereader;

import com.chuiev.exception.FileReadingException;
import org.junit.Test;

/**
 * Tests for {@link AbsolutePathFileReader}.
 *
 * @author Oleksii_Chuiev
 */
public class AbsolutePathFileReaderTest {

    private AbsolutePathFileReader reader = new AbsolutePathFileReader();

    @Test(expected = FileReadingException.class)
    public void shouldFailReadingFileByWrongPath() throws Exception {
        reader.readFile("wrong path");
    }

}
package com.chuiev.filereader;

import com.chuiev.exception.FileReadingException;
import org.junit.Test;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link ResourceFileReader}.
 *
 * @author Oleksii_Chuiev
 */
public class ResourceFileReaderTest {

    private ResourceFileReader reader = new ResourceFileReader();

    @Test(expected = FileReadingException.class)
    public void shouldFailReadingFileByWrongPath() throws Exception {
        reader.readFile("wrong path");
    }

    @Test
    public void shouldReadResourceFile() throws Exception {
        Stream<String> result = reader.readFile("input1-test.txt");

        assertEquals("NUMBERSonethreetwoonethree", result.collect(Collectors.joining()));
    }

}
package com.chuiev.base.matcher;

import com.chuiev.api.CategoryMatcher;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests for {@link CategoryNameMatcher}.
 *
 * @author Oleksii_Chuiev
 */
public class CategoryNameMatcherTest {

    private static final String CATEGORY_NAME = "name";

    private CategoryMatcher matcher = new CategoryNameMatcher(CATEGORY_NAME);

    @Test
    public void shouldMatchesNameInLowerCase() throws Exception {
        assertTrue(matcher.matches(CATEGORY_NAME.toLowerCase()));
    }

    @Test
    public void shouldMatchesNameInUpperCase() throws Exception {
        assertTrue(matcher.matches(CATEGORY_NAME.toUpperCase()));
    }

    @Test
    public void shouldMatchesNameInAnyCase() throws Exception {
        assertTrue(matcher.matches("NamE"));
    }

    @Test
    public void shouldNotMatchesName() throws Exception {
        assertFalse(matcher.matches("Some name"));
    }

    @Test
    public void shouldNotMatchesEmptyName() throws Exception {
        assertFalse(matcher.matches(StringUtils.EMPTY));
    }

    @Test
    public void shouldNotMatchesNullName() throws Exception {
        assertFalse(matcher.matches(null));
    }

}
package com.chuiev.base.registry;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertFalse;

/**
 * Tests for {@link CategoryDescriptorsRegistry}.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryDescriptorsRegistryTest {

    @Autowired
    private CategoryDescriptorsRegistry registry;

    @Test
    public void shouldReturnInstantiatedCategoryDescriptors() throws Exception {
        assertFalse(registry.getCategoryDescriptors().isEmpty());
    }

}
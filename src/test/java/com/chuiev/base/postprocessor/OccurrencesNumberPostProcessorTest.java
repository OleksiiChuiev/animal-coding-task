package com.chuiev.base.postprocessor;

import org.junit.Test;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests for {@link OccurrencesNumberPostProcessor}.
 *
 * @author Oleksii_Chuiev
 */
public class OccurrencesNumberPostProcessorTest {

    private OccurrencesNumberPostProcessor processor = new OccurrencesNumberPostProcessor();

    @Test
    public void shouldCountStatistics() throws Exception {
        Stream<String> result = processor.process(Stream.of("one"));

        assertEquals("one: 1", result.collect(Collectors.joining()));
    }

    @Test
    public void shouldCountStatisticsAndDistinctValues() throws Exception {
        Stream<String> result = processor.process(Stream.of("one", "one"));

        assertEquals("one: 2", result.collect(Collectors.joining()));
    }

    @Test
    public void shouldCountStatisticsForSeveralValues() throws Exception {
        String result = processor.process(Stream.of("one", "two", "one")).collect(Collectors.joining());

        assertTrue(result.contains("one: 2"));
        assertTrue(result.contains("two: 1"));
    }

}
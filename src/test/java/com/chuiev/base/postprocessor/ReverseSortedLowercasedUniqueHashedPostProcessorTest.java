package com.chuiev.base.postprocessor;

import org.junit.Test;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link ReverseSortedLowercasedUniqueHashedPostProcessor}.
 *
 * @author Oleksii_Chuiev
 */
public class ReverseSortedLowercasedUniqueHashedPostProcessorTest {

    private ReverseSortedLowercasedUniqueHashedPostProcessor processor = new ReverseSortedLowercasedUniqueHashedPostProcessor();

    @Test
    public void shouldTurnToLowerCaseValueAndGetHashe() throws Exception {
        Stream<String> result = processor.process(Stream.of("B"));

        assertEquals("b (92eb5ffee6ae2fec3ad71c777531578f)", result.collect(Collectors.joining()));
    }

    @Test
    public void shouldTurnToLowerCaseValueAndGetHasheAndDistinct() throws Exception {
        Stream<String> result = processor.process(Stream.of("B", "B", "B"));

        assertEquals("b (92eb5ffee6ae2fec3ad71c777531578f)", result.collect(Collectors.joining()));
    }

    @Test
    public void shouldTurnToLowerCaseValueAndGetHasheAndSortInReverseOrder() throws Exception {
        Stream<String> result = processor.process(Stream.of("A", "B"));

        assertEquals("b (92eb5ffee6ae2fec3ad71c777531578f)a (0cc175b9c0f1b6a831c399e269772661)",
                result.collect(Collectors.joining()));
    }

    @Test
    public void shouldTurnToLowerCaseValueGetHasheSortInReverseOrderAndDistinctValues() throws Exception {
        Stream<String> result = processor.process(Stream.of("B", "B", "A", "A", "B"));

        assertEquals("b (92eb5ffee6ae2fec3ad71c777531578f)a (0cc175b9c0f1b6a831c399e269772661)",
                result.collect(Collectors.joining()));
    }

}
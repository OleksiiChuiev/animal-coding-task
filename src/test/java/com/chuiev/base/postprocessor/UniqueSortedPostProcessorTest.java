package com.chuiev.base.postprocessor;

import org.junit.Test;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link UniqueSortedPostProcessor}.
 *
 * @author Oleksii_Chuiev
 */
public class UniqueSortedPostProcessorTest {

    private UniqueSortedPostProcessor processor = new UniqueSortedPostProcessor();

    @Test
    public void shouldSortInAscendingOrder() throws Exception {
        Stream<String> result = processor.process(Stream.of("b", "a", "c"));

        assertEquals("abc", result.collect(Collectors.joining()));
    }

    @Test
    public void shouldDistinctValues() throws Exception {
        Stream<String> result = processor.process(Stream.of("b", "b", "b"));

        assertEquals("b", result.collect(Collectors.joining()));
    }

    @Test
    public void shouldDistinctValuesAndSortInAscendingOrder() throws Exception {
        Stream<String> result = processor.process(Stream.of("b", "b", "b", "a", "c"));

        assertEquals("abc", result.collect(Collectors.joining()));
    }

}
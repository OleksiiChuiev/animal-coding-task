package com.chuiev.base.descriptor;

import com.chuiev.base.formatter.CategoryOutputFormatter;
import com.chuiev.base.matcher.CategoryNameMatcher;
import com.chuiev.base.postprocessor.OccurrencesNumberPostProcessor;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests for {@link CategoryDescriptor}.
 *
 * @author Oleksii_Chuiev
 */
public class CategoryDescriptorTest {

    private static final String NAME = "name";

    private CategoryDescriptor descriptor;

    @Before
    public void setUp() throws Exception {
        descriptor = new CategoryDescriptor(
                NAME,
                new CategoryNameMatcher(NAME),
                new CategoryOutputFormatter(NAME),
                new OccurrencesNumberPostProcessor()
        );
    }

    @Test
    public void shouldCorrectlyAddNewValue() throws Exception {
        descriptor.addValue("value");

        assertTrue(descriptor.getValues().contains("value"));
    }

    @Test
    public void shouldMatchCategory() throws Exception {
        assertTrue(descriptor.getCategoryMatcher().matches(NAME));
    }

    @Test
    public void shouldCorrectlyProcessValues() throws Exception {
        descriptor.addValue("value");
        descriptor.addValue("value1");

        String expected = "NAME:" + System.lineSeparator() +
                "value1: 1" + System.lineSeparator() +
                "value: 1";

        assertEquals(expected, descriptor.processValues());
    }

}
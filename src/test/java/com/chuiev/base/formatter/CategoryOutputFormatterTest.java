package com.chuiev.base.formatter;

import org.junit.Test;

import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link CategoryOutputFormatter}.
 *
 * @author Oleksii_Chuiev
 */
public class CategoryOutputFormatterTest {

    private static final String CATEGORY_NAME = "name";

    private CategoryOutputFormatter formatter = new CategoryOutputFormatter(CATEGORY_NAME);

    @Test
    public void shouldOutputOnlyCategoryNameWithLineSeparatorForEmptyStream() {
        // Given
        Stream<String> values = Stream.empty();
        String expected = CATEGORY_NAME.toUpperCase() + ":" + System.lineSeparator();

        // When
        String result = formatter.format(values);

        // Then
        assertEquals(expected, result);
    }

    @Test
    public void shouldOutputCategoryNameAndValuesWithLineSeparators() {
        // Given
        Stream<String> values = Stream.of("1", "2");
        String expected = CATEGORY_NAME.toUpperCase() + ":" +
                System.lineSeparator() + "1" +
                System.lineSeparator() + "2";

        // When
        String result = formatter.format(values);

        // Then
        assertEquals(expected, result);
    }

}
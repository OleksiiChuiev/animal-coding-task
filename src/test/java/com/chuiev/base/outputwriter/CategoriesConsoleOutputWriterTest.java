package com.chuiev.base.outputwriter;

import com.chuiev.base.descriptor.CategoryDescriptor;
import com.chuiev.base.formatter.CategoryOutputFormatter;
import com.chuiev.base.matcher.CategoryNameMatcher;
import com.chuiev.base.postprocessor.UniqueSortedPostProcessor;
import com.chuiev.base.registry.CategoryDescriptorsRegistry;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Tests for {@link CategoriesConsoleOutputWriter}.
 *
 * @author Oleksii_Chuiev
 */
public class CategoriesConsoleOutputWriterTest {

    private static final String DEFAULT_CATEGORY_NAME = "default";
    private static final String ANIMALS_CATEGORY_NAME = "animals";

    private CategoryDescriptor defaultDescriptor;
    private CategoryDescriptor animalsDescriptor;
    private CategoriesConsoleOutputWriter writer;
    private ByteArrayOutputStream bytesStream;
    private CategoryDescriptorsRegistry registry;

    @Before
    public void setUp() throws Exception {
        defaultDescriptor = new CategoryDescriptor(
                DEFAULT_CATEGORY_NAME,
                new CategoryNameMatcher(DEFAULT_CATEGORY_NAME),
                new CategoryOutputFormatter(DEFAULT_CATEGORY_NAME),
                stream -> stream
        );
        animalsDescriptor = new CategoryDescriptor(ANIMALS_CATEGORY_NAME,
                new CategoryNameMatcher(ANIMALS_CATEGORY_NAME),
                new CategoryOutputFormatter(ANIMALS_CATEGORY_NAME),
                new UniqueSortedPostProcessor()
        );
        bytesStream = new ByteArrayOutputStream();
        PrintStream stream = new PrintStream(bytesStream);
        System.setOut(stream);
    }

    @Test
    public void outputCategories() throws Exception {
    }

    @Test
    public void shouldNotOutputCategoriesForEmptyListOfDescriptors() throws Exception {
        // Given
        registry = new CategoryDescriptorsRegistry(Collections.emptyList());
        writer = new CategoriesConsoleOutputWriter(registry);

        // When
        writer.outputCategories();

        // Then
        assertTrue(bytesStream.toString().isEmpty());
    }

    @Test
    public void shouldNotOutputCategoriesForEmptyCategoriesValues() throws Exception {
        // Given
        registry = new CategoryDescriptorsRegistry(Arrays.asList(defaultDescriptor, animalsDescriptor));
        writer = new CategoriesConsoleOutputWriter(registry);

        // When
        writer.outputCategories();

        // Then
        assertTrue(bytesStream.toString().isEmpty());
    }

    @Test
    public void shouldOutputFormattedAndProcessedAnimalsCategoryValues() throws Exception {
        // Given
        animalsDescriptor.addValue("cat");
        registry = new CategoryDescriptorsRegistry(Collections.singletonList(animalsDescriptor));
        writer = new CategoriesConsoleOutputWriter(registry);

        // When
        writer.outputCategories();

        // Then
        assertEquals(animalsDescriptor.getName().toUpperCase() + ":" +
                        System.lineSeparator() +
                        "cat" +
                        System.lineSeparator(),
                bytesStream.toString());
    }

    @Test
    public void shouldOutputFormattedAndProcessedAnimalsAndDefaultCategoryValues() throws Exception {
        // Given
        animalsDescriptor.addValue("cat");
        defaultDescriptor.addValue("value");
        registry = new CategoryDescriptorsRegistry(Arrays.asList(defaultDescriptor, animalsDescriptor));
        writer = new CategoriesConsoleOutputWriter(registry);

        // When
        writer.outputCategories();
        String output = bytesStream.toString();

        // Then
        assertTrue(output.contains(animalsDescriptor.getName().toUpperCase() + ":" +
                System.lineSeparator() +
                "cat" +
                System.lineSeparator()));
        assertTrue(output.contains(defaultDescriptor.getName().toUpperCase() + ":" +
                System.lineSeparator() +
                "value" +
                System.lineSeparator()));
    }

}
package com.chuiev.base.filler;

import com.chuiev.base.descriptor.CategoryDescriptor;
import com.chuiev.base.formatter.CategoryOutputFormatter;
import com.chuiev.base.matcher.CategoryNameMatcher;
import com.chuiev.base.postprocessor.UniqueSortedPostProcessor;
import com.chuiev.base.registry.CategoryDescriptorsRegistry;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests for {@link BaseCategoriesFiller}.
 *
 * @author Oleksii_Chuiev
 */
public class BaseCategoriesFillerTest {

    private static final String DEFAULT_CATEGORY_NAME = "default";
    private static final String ANIMALS_CATEGORY_NAME = "animals";

    private CategoryDescriptor defaultDescriptor;
    private CategoryDescriptor animalsDescriptor;
    private BaseCategoriesFiller filler;
    private CategoryDescriptorsRegistry registry;

    @Before
    public void setUp() throws Exception {
        defaultDescriptor = new CategoryDescriptor(
                DEFAULT_CATEGORY_NAME,
                new CategoryNameMatcher(DEFAULT_CATEGORY_NAME),
                new CategoryOutputFormatter(DEFAULT_CATEGORY_NAME),
                stream -> stream
        );
        animalsDescriptor = new CategoryDescriptor(ANIMALS_CATEGORY_NAME,
                new CategoryNameMatcher(ANIMALS_CATEGORY_NAME),
                new CategoryOutputFormatter(ANIMALS_CATEGORY_NAME),
                new UniqueSortedPostProcessor()
        );
    }

    @Test
    public void shouldFillLineToDefaultDescriptor() throws Exception {
        // Given
        registry = new CategoryDescriptorsRegistry(Collections.emptyList());
        filler = new BaseCategoriesFiller(registry, defaultDescriptor);
        String line = "some value";

        // When
        filler.fillCategoryByLine(line);

        // Then
        assertTrue(defaultDescriptor.getValues().contains(line));
    }

    @Test
    public void shouldNotFillNullLineToDefaultDescriptor() throws Exception {
        // Given
        registry = new CategoryDescriptorsRegistry(Collections.emptyList());
        filler = new BaseCategoriesFiller(registry, defaultDescriptor);

        // When
        filler.fillCategoryByLine(null);

        // Then
        assertTrue(defaultDescriptor.getValues().isEmpty());
    }

    @Test
    public void shouldNotFillEmptyLineToDefaultDescriptor() throws Exception {
        // Given
        registry = new CategoryDescriptorsRegistry(Collections.emptyList());
        filler = new BaseCategoriesFiller(registry, defaultDescriptor);

        // When
        filler.fillCategoryByLine(StringUtils.EMPTY);

        // Then
        assertTrue(defaultDescriptor.getValues().isEmpty());
    }

    @Test
    public void shouldNotFillDefaultCategoryNameToDefaultDescriptor() throws Exception {
        // Given
        registry = new CategoryDescriptorsRegistry(Collections.emptyList());
        filler = new BaseCategoriesFiller(registry, defaultDescriptor);

        // When
        filler.fillCategoryByLine("default");

        // Then
        assertTrue(defaultDescriptor.getValues().isEmpty());
    }

    @Test
    public void shouldNotAddAnimalsCategoryNameAsValueToAnyDescriptor() throws Exception {
        // Given
        registry = new CategoryDescriptorsRegistry(Arrays.asList(defaultDescriptor, animalsDescriptor));
        filler = new BaseCategoriesFiller(registry, defaultDescriptor);
        String line = "animals";

        // When
        filler.fillCategoryByLine(line);

        // Then
        assertTrue(defaultDescriptor.getValues().isEmpty());
        assertTrue(animalsDescriptor.getValues().isEmpty());
    }

    @Test
    public void shouldAddValueToAnimalsDescriptorAfterChangingCurrentDescriptor() throws Exception {
        // Given
        registry = new CategoryDescriptorsRegistry(Arrays.asList(defaultDescriptor, animalsDescriptor));
        filler = new BaseCategoriesFiller(registry, defaultDescriptor);
        String line1 = "animals";
        String line2 = "value";

        // When
        filler.fillCategoryByLine(line1);
        filler.fillCategoryByLine(line2);

        // Then
        assertTrue(defaultDescriptor.getValues().isEmpty());
        assertTrue(animalsDescriptor.getValues().contains(line2));
        assertFalse(animalsDescriptor.getValues().contains(line1));
    }

}
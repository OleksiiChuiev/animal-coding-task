package com.chuiev;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Integration tests for whole application.
 * The main purpose of these tests is to check whether whole application works correctly,
 * separate parts works fine together.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AppTest {

    @Test
    public void shouldCorrectlyProcessPredefinedFile() throws Exception {
        // Given
        ByteArrayOutputStream bytesStream = new ByteArrayOutputStream();
        PrintStream stream = new PrintStream(bytesStream);
        System.setOut(stream);

        String animalsPart = "ANIMALS:" + System.lineSeparator() +
                "cow" + System.lineSeparator() +
                "horse" + System.lineSeparator() +
                "moose" + System.lineSeparator() +
                "sheep";
        String numbersPart = "NUMBERS:" + System.lineSeparator() +
                "six: 2" + System.lineSeparator() +
                "one: 2" + System.lineSeparator() +
                "seven: 1" + System.lineSeparator() +
                "two: 1" + System.lineSeparator() +
                "three: 2";
        String carsPart = "CARS:" + System.lineSeparator() +
                "vw (7336a2c49b0045fa1340bf899f785e70)" + System.lineSeparator() +
                "opel (f65b7d39472c52142ea2f4ea5e115d59)" + System.lineSeparator() +
                "bmw (71913f59e458e026d6609cdb5a7cc53d)" + System.lineSeparator() +
                "audi (4d9fa555e7c23996e99f1fb0e286aea8)";

        // When
        App.main(new String[]{});
        String consoleOutput = bytesStream.toString();

        //Then
        assertTrue(consoleOutput.contains(animalsPart));
        assertTrue(consoleOutput.contains(numbersPart));
        assertTrue(consoleOutput.contains(carsPart));

        assertEquals(animalsPart + System.lineSeparator() +
                carsPart + System.lineSeparator() +
                numbersPart + System.lineSeparator(),
                consoleOutput);
    }
}

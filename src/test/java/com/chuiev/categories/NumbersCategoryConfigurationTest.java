package com.chuiev.categories;

import com.chuiev.base.descriptor.CategoryDescriptor;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link NumbersCategoryConfiguration} which aims to tests correctness
 * of instantiating numbers descriptor.
 *
 * @author Oleksii_Chuiev
 */
public class NumbersCategoryConfigurationTest {

    private CategoryDescriptor descriptor;

    @Before
    public void setUp() throws Exception {
        descriptor = new NumbersCategoryConfiguration().numbersDescriptor();
    }

    @Test
    public void shouldCorrectlyProcessValuesForNumbersCategoryDescriptor() throws Exception {
        descriptor.addValue("value");
        descriptor.addValue("value1");

        String expected = "NUMBERS:" + System.lineSeparator() +
                "value1: 1" + System.lineSeparator() +
                "value: 1";

        assertEquals(expected, descriptor.processValues());
    }

}
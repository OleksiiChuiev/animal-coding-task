package com.chuiev.categories;

import com.chuiev.base.descriptor.CategoryDescriptor;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link DefaultCategoryConfiguration} which aims to tests correctness
 * of instantiating default descriptor.
 *
 * @author Oleksii_Chuiev
 */
public class DefaultCategoryConfigurationTest {

    private CategoryDescriptor descriptor;

    @Before
    public void setUp() throws Exception {
        descriptor = new DefaultCategoryConfiguration().defaultDescriptor();
    }

    @Test
    public void shouldCorrectlyProcessValuesForDefaultCategoryDescriptor() throws Exception {
        descriptor.addValue("value");
        descriptor.addValue("value1");

        String expected = "DEFAULT:" + System.lineSeparator() +
                "value" + System.lineSeparator() +
                "value1";

        assertEquals(expected, descriptor.processValues());
    }

}
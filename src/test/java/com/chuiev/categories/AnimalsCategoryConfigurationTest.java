package com.chuiev.categories;

import com.chuiev.base.descriptor.CategoryDescriptor;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link AnimalsCategoryConfiguration} which aims to tests correctness
 * of instantiating animals descriptor.
 *
 * @author Oleksii_Chuiev
 */
public class AnimalsCategoryConfigurationTest {

    private CategoryDescriptor descriptor;

    @Before
    public void setUp() throws Exception {
        descriptor = new AnimalsCategoryConfiguration().animalsDescriptor();
    }

    @Test
    public void shouldCorrectlyProcessValuesForAnimalsCategoryDescriptor() throws Exception {
        descriptor.addValue("value");
        descriptor.addValue("value1");

        String expected = "ANIMALS:" + System.lineSeparator() +
                "value" + System.lineSeparator() +
                "value1";

        assertEquals(expected, descriptor.processValues());
    }

}
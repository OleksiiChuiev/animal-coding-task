package com.chuiev.categories;

import com.chuiev.base.descriptor.CategoryDescriptor;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link CarsCategoryConfiguration} which aims to tests correctness
 * of instantiating cars descriptor.
 *
 * @author Oleksii_Chuiev
 */
public class CarsCategoryConfigurationTest {

    private CategoryDescriptor descriptor;

    @Before
    public void setUp() throws Exception {
        descriptor = new CarsCategoryConfiguration().carsDescriptor();
    }

    @Test
    public void shouldCorrectlyProcessValuesForCarsCategoryDescriptor() throws Exception {
        descriptor.addValue("value");
        descriptor.addValue("value1");

        String expected = "CARS:" + System.lineSeparator() +
                "value1 (9946687e5fa0dab5993ededddb398d2e)" + System.lineSeparator() +
                "value (2063c1608d6e0baf80249c42e2be5804)";

        assertEquals(expected, descriptor.processValues());
    }

}